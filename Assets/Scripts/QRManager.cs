﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZXing;
using ZXing.QrCode;
using UnityEngine.UI;

/// <summary>
/// Responsible for the interaction with the camera the QrCode logique
/// </summary>
public class QRManager : MonoBehaviour
{
    /// <summary>
    /// The texture stream coming from the camera
    /// </summary>
    private WebCamTexture camTexture;

    /// <summary>
    /// The text decoded from the QRCode
    /// </summary>
    public string QRText;

    /// <summary>
    /// The plane used to show the camera stream on the application view
    /// </summary>
    public GameObject camTextureGameobject;

    /// <summary>
    /// Called at startup
    /// Create the stream to the camera with the right Height and Width
    /// Link the plane view material texture to the camera stream texture
    /// </summary>
    void Start()
    {
        camTexture = new WebCamTexture();
        camTexture.requestedHeight = Screen.height;
        camTexture.requestedWidth = Screen.width;
        camTextureGameobject.GetComponent<Image>().material.mainTexture = camTexture;
        QRText = "";
    }

    /// <summary>
    /// Linked to the plane screen application view on click/touch
    /// Call the ZXing library to try to decode the frame.
    /// If a QR Code is decoded, process the creation of a new promo code request with the decoded url.
    /// </summary>
    public void renderCamera()
    {
        if (camTexture != null)
        {
            if(camTexture.isPlaying)
            {
                try
                {
                    IBarcodeReader barcodeReader = new BarcodeReader();
                    // decode the current frame
                    var result = barcodeReader.Decode(camTexture.GetPixels32(),
                      camTexture.width, camTexture.height);
                    if (result != null)
                    {
                        QRText = result.Text;
                        Debug.Log("DECODED TEXT FROM QR: " + result.Text);
                        camTexture.Stop();
                        StartCoroutine(getPromoCodeFromServer(result.Text));
                    }
                    else
                    {
                        Debug.Log("NO QR CODE FOUND");
                    }
                }
                catch (Exception ex) { Debug.LogWarning(ex.Message); }
            }
            else
            {
                camTexture.Play();
                GameObject.FindGameObjectWithTag("Selenium").GetComponent<SetupSelenium>().setSeleniumButtonText("QRCodeCamera OK");
            }
        }
    }

    /// <summary>
    /// Responsible for calling an http request with the passed string url in order to create a new promo code.
    /// </summary>
    /// <param name="result">The decoded string from a QR Code</param>
    public IEnumerator getPromoCodeFromServer(string result)
    {
        gameObject.GetComponent<HttpManager>().httpGetPromoCode(result);

        yield return null;
    }

    /// <summary>
    /// Called every frame
    /// Listen for the device rotation to adapte the plane gameobject.
    /// </summary>
    void OnGUI()
    {
        if (camTexture != null)
        {
            if (camTexture.isPlaying)
            {
                camTextureGameobject.transform.rotation = Quaternion.Euler(camTextureGameobject.transform.rotation.x, camTextureGameobject.transform.rotation.y, -camTexture.videoRotationAngle);
            }
        }
    }
}
