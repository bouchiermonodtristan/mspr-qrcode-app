﻿using CI.HttpClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

/// <summary>
/// Represents the access to our API server.
/// </summary>
public class HttpManager : MonoBehaviour
{
    /// <summary>
    /// The link to the scene object parent of the promo codes.
    /// This reference will be used to access the promo codes data script.
    /// </summary>
    public GameObject promoCodeGameObject;

    /// <summary>
    /// Launch an http request get on the passed string url
    /// Try to ask PromoCodeData to add the unserialized promo code.
    /// </summary>
    /// <param name="serverEndpoint">The uncrypted URL passed as string</param>
    public void httpGetPromoCode(string serverEndpoint)
    {
        HttpClient client = new HttpClient();

        client.Get(new Uri(serverEndpoint), HttpCompletionOption.AllResponseContent, (r) =>
        {
            try
            {
                string readAsString = r.ReadAsString();
                PromoCodeModel result = createModel(readAsString);
                promoCodeGameObject.GetComponent<PromoCodeData>().addPromoCode(result);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        });
    }

    /// <summary>
    /// Create the right promo code model based on a json string
    /// </summary>
    /// <param name="inputJson">The json with al the required datas for our code objects</param>
    /// <returns>The model completed</returns>
    private PromoCodeModel createModel(string inputJson)
    {
        string code = "";
        string conditions = "";
        string dateEnd = "";
        string dateStart = "";
        string leftCoupons = "";
        string name = "";
        string url = "";
        string brand = "";

        string codePattern = "\"code\":\"(.+?)\",";
        Match match = Regex.Match(inputJson, codePattern);
        if (match.Success)
        {
            code = match.Groups[1].Value;
        }
        string conditionsPattern = "\"conditions\":\"(.+?)\",";
        match = Regex.Match(inputJson, conditionsPattern);
        if (match.Success)
        {
            conditions = match.Groups[1].Value;
        }
        string dateEndPattern = "\"dateEnd\":\"(.+?)\",";
        match = Regex.Match(inputJson, dateEndPattern);
        if (match.Success)
        {
            dateEnd = match.Groups[1].Value;
        }
        string dateStartPattern = "\"dateStart\":\"(.+?)\",";
        match = Regex.Match(inputJson, dateStartPattern);
        if (match.Success)
        {
            dateStart = match.Groups[1].Value;
        }
        string leftCouponsPattern = "\"leftCoupons\":\"(.+?)\",";
        match = Regex.Match(inputJson, leftCouponsPattern);
        if (match.Success)
        {
            leftCoupons = match.Groups[1].Value;
        }
        string namePattern = "\"name\":\"(.+?)\"";
        match = Regex.Match(inputJson, namePattern);
        if (match.Success)
        {
            name = match.Groups[1].Value;
        }
        string urlPattern = "\"url\":\"(.+?)\",";
        match = Regex.Match(inputJson, urlPattern);
        if (match.Success)
        {
            url = match.Groups[1].Value;
        }
        string brandPattern = "\"brand\":\"(.+?)\"";
        match = Regex.Match(inputJson, brandPattern);
        if (match.Success)
        {
            brand = match.Groups[1].Value;
        }

        string ButtonTittle = code;
        string ModalTittle = name;
        string ModalFirstLine = conditions;
        string ModalSecondLine = url;

        return new PromoCodeModel(ButtonTittle, ModalTittle, ModalFirstLine, ModalSecondLine);
    }

    /// <summary>
    /// Request an url passed as a parameter
    /// </summary>
    /// <param name="url">The url required as a string</param>
    public void ManuallyGetCodeButton(string url)
    {
        httpGetPromoCode(url);
        GameObject.FindGameObjectWithTag("Selenium").GetComponent<SetupSelenium>().setSeleniumButtonText("HttpManager OK");
    }
}
