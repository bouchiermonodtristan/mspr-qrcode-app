Used Assets and Documentation links :

Integration tests : https://altom.gitlab.io/altunity/altunitytester/
AltUnity Tester is an open-source UI driven test automation tool that helps you find objects in your game and interacts with them using tests written in C#, Python or Java. You can run your tests on real devices (mobile, PCs, etc.) or inside the Unity Editor.

HTTP : https://github.com/ClaytonIndustries/HttpClient/wiki
Clayton HTTP asset is an alternative to WebHTTPRequest from MonoBehavior.

UI Design : https://static.wixstatic.com/ugd/a8f640_847c5fe28f1f4e70b3b6e4a9733f1032.pdf
Modern UI Pack is a package made by Michsky. It's build above unity default UI, and provide all widgets required to build our application on.

Mobile console : https://assetstore.unity.com/packages/tools/utilities/mobile-console-108594
A debug tool provided bu Marcus Lagerströme, in order to print a debug windows on our mobile builds.
Just tap on the top corner of the builded apllication.








